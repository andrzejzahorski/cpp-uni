cmake_minimum_required(VERSION 3.6)
project(Wyklady)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES w03/main.cpp)
add_executable(Wyklady ${SOURCE_FILES})