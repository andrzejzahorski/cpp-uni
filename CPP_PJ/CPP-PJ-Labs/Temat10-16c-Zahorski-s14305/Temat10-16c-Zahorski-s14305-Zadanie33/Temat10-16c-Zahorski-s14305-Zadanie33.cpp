#include <iostream>
using namespace std;
#include "Odcinek.h"

int main()
{
    Odcinek* o1 = new Odcinek;
    o1->drukuj();
    Odcinek* o2 = new Odcinek(Punkt(1,2), Punkt(2,2));
    o2->drukuj();
    Odcinek* o3 = new Odcinek(*o2);
    o3->drukuj();

    Odcinek* rzutX = o2->rzutX();
    Odcinek* rzutY = o2->rzutY();

    rzutX -> drukuj();
    rzutY -> drukuj();

    delete o1;
    delete o2;
    delete o3;
    delete rzutX;
    delete rzutY;
    return 0;
}