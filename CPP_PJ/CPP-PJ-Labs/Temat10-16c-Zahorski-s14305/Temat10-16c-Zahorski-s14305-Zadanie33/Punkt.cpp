//
// Created by Andy on 5/24/17.
//

#include <cmath>
#include "Punkt.h"
#include <iostream>
using namespace std;
int Punkt::globalCounter = 0;
Punkt::Punkt()
{
//    cout << __PRETTY_FUNCTION__ << endl;
    globalCounter++;
    nr = globalCounter;
    p_arr = new int[2];
    p_arr[0] = 0;
    p_arr[1] = 0;
}

Punkt::Punkt(int x, int y)
{
//    cout << __PRETTY_FUNCTION__ << endl;
    globalCounter++;
    nr = globalCounter;
    p_arr = new int[2];
    p_arr[0] = x;
    p_arr[1] = y;
}

Punkt::Punkt(const Punkt& p)
{
    //cout << __PRETTY_FUNCTION__ << endl;
    globalCounter++;
    nr = globalCounter;
    p_arr = new int[2];
    p_arr[0] = p.p_arr[0];
    p_arr[1] = p.p_arr[1];
}

Punkt::~Punkt()
{
 //   cout << __PRETTY_FUNCTION__ << endl;
    delete[] p_arr;
}
//co zwraca skąd  :: co biorę
int Punkt::wspX() const
{
    return p_arr[0];
}

int Punkt::wspY() const
{
    return p_arr[1];
}

void Punkt::ustaw(int x, int y)
{
    p_arr[0] = x;
    p_arr[1] = y;
}

double Punkt::odl(const Punkt &p)
{
    return sqrt(pow(p_arr[0] - p.p_arr[0],2) +  pow(p_arr[1] - p.p_arr[1],2));
}

void Punkt::drukuj() const
{
    cout << "Punkt nr:" << nr << " X: " << p_arr[0] << "  Y: " << p_arr[1] << endl;
}

