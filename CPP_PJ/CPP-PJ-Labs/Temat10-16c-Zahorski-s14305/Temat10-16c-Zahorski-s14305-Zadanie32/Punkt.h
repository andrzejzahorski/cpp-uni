

#ifndef TEMAT10_PUNKT_H
#define TEMAT10_PUNKT_H


class Punkt
{
public:
    Punkt();
    Punkt(int x, int y);
    Punkt(const Punkt& p);
    ~Punkt();

    int wspX() const;
    int wspY() const;
    void ustaw(int x, int y);
    double odl(const Punkt &p);
    void drukuj() const;
private:
    static int globalCounter;
    int nr;
    int* p_arr;
};


#endif //TEMAT10_PUNKT_H
