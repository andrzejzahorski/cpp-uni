#include <iostream>
#include "Punkt.h"

int main()
{
    Punkt p1;
    Punkt p2 = Punkt();
    Punkt p3(3,3);
    Punkt p4 = Punkt(4,4);
    Punkt p5 = p4;
    Punkt p6 = Punkt(p5);
    Punkt p7(p6);

    Punkt *p8 = new Punkt(8,8);
    Punkt*p9 = new Punkt(*p8);
    p1.drukuj();
    p2.drukuj();
    p3.drukuj();
    p4.drukuj();
    p5.drukuj();
    p6.drukuj();
    p7.drukuj();
    p8 -> drukuj();
    p9 -> drukuj();

    delete p8;
    delete p9;

    return 0;
}