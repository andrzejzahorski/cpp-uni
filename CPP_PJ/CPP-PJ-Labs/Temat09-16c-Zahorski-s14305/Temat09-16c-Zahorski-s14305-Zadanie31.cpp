#include <iostream>

struct Node
{
    int value;
    struct Node* next;
};

struct List
{
    struct Node *first;
    struct Node *last;
};

List* list()
{
    List* ptr_list = new List;
    ptr_list->first = NULL;
    ptr_list->last = NULL;

    return ptr_list;
}

Node* node(int v)
{
    Node* ptr_node = new Node;
    ptr_node->next = NULL;
    ptr_node->value = v;
    return ptr_node;
}

List* push(List* ptr_list, Node* ptr_node)
{
    Node* ptr_tmp = ptr_list->first;
    while(ptr_tmp != NULL)
    {
        if(ptr_tmp->value == ptr_node->value)
            return ptr_list;

        ptr_tmp = ptr_tmp->next;
    }

    ptr_node->next = ptr_list->first;
    if(!ptr_list->first)
        ptr_list->last = ptr_node;
    ptr_list->first = ptr_node;

    return ptr_list;
}

List* inject(List* ptr_list, Node* ptr_node)
{
    Node* ptr_tmp = ptr_list->first;
    while(ptr_tmp != NULL)
    {
        if(ptr_tmp->value == ptr_node->value)
            return ptr_list;

        ptr_tmp = ptr_tmp->next;
    }

    if(ptr_list->last != NULL)
        ptr_list->last->next = ptr_node;
    else
        ptr_list->first = ptr_node;

    ptr_list->last = ptr_node;

    return ptr_list;
}
List* insertAfter(List* ptr_list, Node* ptr_current, Node* ptr_node)
{
    Node* ptr_tmp = ptr_list->first;
    while(ptr_tmp != NULL)
    {
        if(ptr_tmp->value == ptr_node->value)
            return ptr_list;

        ptr_tmp = ptr_tmp->next;
    }

    if(ptr_current == ptr_list->last)
        ptr_list->last = ptr_node;
    ptr_node->next = ptr_current->next;
    ptr_current->next = ptr_node;

    return ptr_list;
}

List* insertBefore(List* ptr_list, Node* ptr_current, Node* ptr_node)
{
    Node* ptr_tmp = ptr_list->first;
    Node* ptr_before_current = NULL;
    while(ptr_tmp != NULL)
    {
        if(ptr_tmp->value == ptr_node->value)
            return ptr_list;

        if(ptr_tmp->next == ptr_current)
            ptr_before_current = ptr_tmp;

        ptr_tmp = ptr_tmp->next;
    }

    if(ptr_current == ptr_list->first)
        ptr_list->first = ptr_node;
    else
        ptr_before_current->next = ptr_node;
    ptr_node->next = ptr_current;

    return ptr_list;
}

List* del(List* ptr_list, int v)
{
    Node* ptr_tmp = ptr_list->first;
    Node* ptr_before_tmp = NULL;
    while(ptr_tmp != NULL)
    {
        if(ptr_tmp->value == v)
        {
            if(ptr_tmp == ptr_list->first)
                ptr_list->first = ptr_tmp->next;
            else
                ptr_before_tmp->next = ptr_tmp->next;

            if(ptr_tmp == ptr_list->last)
                ptr_list->last = ptr_before_tmp;

            delete ptr_tmp;
            return ptr_list;
        }

        ptr_before_tmp = ptr_tmp;
        ptr_tmp = ptr_tmp->next;
    }

    return ptr_list;
}

void printNode(Node* ptr_node)
{
    std::cout << ptr_node->value << std::endl;
}

void printLR(List* ptr_list)
{
    Node* ptr_tmp = ptr_list->first;
    while(ptr_tmp != NULL)
    {
        printNode(ptr_tmp);

        ptr_tmp = ptr_tmp->next;
    }
}

void recursivePrint(Node* ptr_node)
{
    if(!ptr_node)
        return;

    recursivePrint(ptr_node->next);
    printNode(ptr_node);
};

void printRL(List* ptr_list)
{
    recursivePrint(ptr_list->first);
}
/* wracającą listę węzłów zawierających wszystkie liczby pierwsze z przedziału [2,n] metodą sita Eratostenesa.
 * Należy skorzystać wyłącznie z operacji na liście.
*/

List* sieve(int n)
{
}

// 3 9 5 7 8

void test() {
    List *ptr_list = list();

    int num = 0;
    Node *ptr_node = NULL;

    num = 1;
    ptr_node = node(num);
    std::cout << "push " << num << ": " << std::endl;
    ptr_list = push(ptr_list, ptr_node);
    printLR(ptr_list);

    num = 2;
    ptr_node = node(num);
    std::cout << "push " << num << ": " << std::endl;
    ptr_list = push(ptr_list, ptr_node);
    printLR(ptr_list);

    num = 3;
    ptr_node = node(num);
    std::cout << "push " << num << ": " << std::endl;
    ptr_list = push(ptr_list, ptr_node);
    printLR(ptr_list);

    num = 4;
    ptr_node = node(num);
    Node *ptr_1 = ptr_node;
    std::cout << "inject " << num << ": " << std::endl;
    ptr_list = inject(ptr_list, ptr_node);
    printLR(ptr_list);

    num = 0;
    ptr_node = node(num);
    std::cout << "insertBefore " << num << ": " << std::endl;
    ptr_list = insertBefore(ptr_list, ptr_1, ptr_node);
    printLR(ptr_list);

    num = 100;
    ptr_node = node(num);
    std::cout << "insertAfter  " << num << ": " << std::endl;
    ptr_list = insertAfter(ptr_list, ptr_1, ptr_node);
    printLR(ptr_list);


    num = 100;
    std::cout << "del  " << num << ": " << std::endl;
    ptr_list = del(ptr_list, num);
    printLR(ptr_list);

    num = 3;
    std::cout << "del  " << num << ": " << std::endl;
    ptr_list = del(ptr_list, num);
    printLR(ptr_list);


    num = 1;
    std::cout << "del  " << num << ": " << std::endl;
    ptr_list = del(ptr_list, num);
    printLR(ptr_list);

    std::cout << "PrintRL  " <<  ": " << std::endl;
    printRL(ptr_list);
}



int main()
{
    std::cout << "test 1" << std::endl;

    test();

    return 0;
}
