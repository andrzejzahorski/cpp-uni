//
// Created by andy on 17.03.17.
//
#include <iostream>

using namespace std;

int main()
{
    int v1 = 1, v2 = 2; // przypisanie wartosci zmiennym

    int *p1 = &v1, *p2 = &v2; //przypisanie adresow do wskaznikow

    cout << "v1 = " << v1 << endl; // wyswietlenie wartosci v1

    cout << "&v1 = " << &v1 << endl; //wyswietlenie adresu v1

    cout << "&v1 = &*p1 = " << &*p1 << endl; // bezsensowna dereferencja wkaznika na adres ktory on juz sam pokazuje

    cout << "p1 = " << p1 << endl; // wyswietlenie adresu pokazywanego przez wskaznik

    cout << "&p1 = " << &p1 << endl; //wyswietlenie adresu wskaznika

    cout << "*p1 = " << *p1 << endl; //wyswietlenie wartosci znajdujacej sie pod danym adresem

//    *p1 = 3; // do wskaznika nie mozna przypisac wartosci chyba ze jest zaalokowana mu pamiec przez operator new
    // niektore kompilatory to przyjma (np moj) i wpisza wartosc w losowe miejsce
    cout << "*p1 = " << *p1 << ", " << "v1 = " << v1 << endl; // wyswietlenie wartosci poprzez dereferencje wskaznika oraz poprzez wartosc

    p1 = p2; // przypisanie adresu liczby pokazywanej przez p2 do p1
    cout << "*p1 = " << *p1 << ", " << "v2 = " << v2 << endl; // efekt tej operacji

    p2 = &v1; // przywrocenie stanu poprzedniego
    cout << "*p2 = " << *p2 << ", " << "v1 = " << v1 << endl; // efekt przywrocenia

    // p1 = 0;// lub: p1 = nullptr; w standardzie C+11    // Wskaźniki nie mogą pokazywać NULL
    // cout << "*p1 = " << *p1 << endl;

    return 0;
}
