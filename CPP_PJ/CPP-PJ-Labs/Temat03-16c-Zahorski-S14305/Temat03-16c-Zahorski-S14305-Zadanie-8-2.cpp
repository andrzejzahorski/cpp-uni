#include <iostream>

using namespace std;

void add1(double);  //  // bez adresow nie dziala globalnie wiec wartosc sie nie zmieni poza funkcja
void add2(double&);// funkcja przyjmuja referencje double zrobi rzeczy globalnie

int main()
{
	double v1 = 1, &o1 = v1, *p = &o1; // deklaracja i inicjalizacja zmiennej v1, zreferencji zmiennej o1 wartoscia v1, oraz wpisanie adresu z o1 do wartosci wskazywanej przez p


	cout << "&v1 = " << &v1 << endl; //adres v1
	cout << "&o1 = " << &o1 << endl; //adres zmiennej o1
	cout << "p = " << p << endl; // adres wskaznika

	cout << "v1 = " << v1 << endl; // wartosc v1
	cout << "o1 = " << o1 << endl;// wartosc o1

	o1 = 2; // przypisanie wartosci zmiennej o2
	// W SKROCIE: JEZELI SIE ZMIENI o2 lub v1 to zmienia sie obie poniewaz operuja na tym samym adresie a wskaznik p pokazuje na adres o1
	cout << "v1 = " << v1 << endl;
	cout << "o1 = " << o1 << endl;
	cout << "*p = " << *p << endl; // wartosc 2

	v1 = 3;

	cout << "v1 = " << v1 << endl;
	cout << "o1 = " << o1 << endl;
	cout << "*p = " << *p << endl;

//	int &o2; // nieużyta refernecja

	const int &o3 = 4;
	cout << "o3 = " << o3 << endl;

	add1(v1);
	cout << "ooo1 = " << o1 << ", v1 = " << v1 << endl;

	add2(o1);
	cout << "o1 = " << o1 << ", v1 = " << v1 << endl;

    return 0;
}

void add1(double x) {
	x = x + 1;
}

void add2(double& x) {
	x = x + 2;
}
