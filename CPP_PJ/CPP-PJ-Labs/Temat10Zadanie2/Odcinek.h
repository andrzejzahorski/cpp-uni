//
// Created by Andy on 5/24/17.
//

#ifndef TEMAT10ZADANIE2_ODCINEK_H
#define TEMAT10ZADANIE2_ODCINEK_H
#include "Punkt.h"


class Odcinek
{
public:
    Odcinek();
    Odcinek(const Punkt& p, const Punkt& k);
    Odcinek(const Odcinek& o);
    ~Odcinek();
    Odcinek* rzutX();
    Odcinek* rzutY();
    void drukuj() const;


private:
    static int globalCounter;
    int nr;
    Punkt p1;
    Punkt p2;

};


#endif //TEMAT10ZADANIE2_ODCINEK_H
