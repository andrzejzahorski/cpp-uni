//
// Created by Andy on 5/24/17.
//

#include <iostream>
#include "Odcinek.h"
int Odcinek::globalCounter = 0;

Odcinek::Odcinek()  : p1(0,0), p2(0,0), nr(++globalCounter) {}
Odcinek::Odcinek(const Punkt& p, const Punkt& k) : p1(p), p2(k), nr(++globalCounter){}
Odcinek::Odcinek(const Odcinek& o) : p1(o.p1), p2(o.p2), nr(++globalCounter){}

Odcinek::~Odcinek()
{
    std::cout << "Zniszczono odcinek nr: " << nr << std::endl;
}

Odcinek* Odcinek::rzutX()
{
    Punkt a(p1.wspX(),0);
    Punkt b(p2.wspX(),0);
    return new Odcinek(a,b);
}

Odcinek* Odcinek::rzutY()
{
    Punkt a(0,p1.wspY());
    Punkt b(0,p2.wspY());
    return new Odcinek(a,b);
}

void Odcinek::drukuj() const
{
    std::cout << "Odcinek nr: " << nr << " {\n";
    p1.drukuj();
    p2.drukuj();
    std::cout << "}\n";
}