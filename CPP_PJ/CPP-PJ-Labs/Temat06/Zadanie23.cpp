

#include <iostream>
#include <cstdarg>
#include <math.h>
double povalue(double x, int n, ...);
void multiply(int* coeff1, int m, int* coeff2, int n, int* result);


double povalue(double x, int n, ...)
{
    double output = 0;
    va_list ap;
    va_start(ap, n);
    for ( int i = 0; i < n; i++ )
    {
            double pow_base = n - 1 - i;
            // std::cout << "pow_base  " << pow_base << '\n';
            double now = va_arg(ap,double);
            if (n - 1 - i >= 1)
            {
                double pow_arg = pow(x, pow_base);
                // std::cout << "pow_arg" << pow_arg << '\n';
                output += now * pow_arg;
            }
            else
            {
                output += now;
            }
    }
  va_end ( ap );

  return output;
}
/* b) void multiply(int* coeff1, int m, int* coeff2, int n, int* result)
 wykonującą operację mnożenia dwóch wielomianów zmiennej x, podanych za
 pośrednictwem tablic współczynników coeff1, coeff2. Współczynniki wielomianu
 wynikowego są zapisane w tablicy result. */
 void multiply(int* coeff1, int m, int* coeff2, int n, int* result)
{
    for (int i=0; i<m; i++)
    {
        for (int j = 0; j < n; j++)
            result[i + j] += coeff1[i] * coeff2[j];
    }

    printWiel(result,m+n-1);

}

int main(int argc, char const *argv[])
{
    double o1 = povalue(-1.0, 3, 4.0, -2.0, 1.0);
    int w1[] = {1 , 2, 3};
    int w2[] = {4, 5, 6};
    int length1 = sizeof(w1)/sizeof(w1[0]);
    int length2 = sizeof(w2)/sizeof(w2[0]);
    std::cout << "PODAJ X  " << '\n';
    int x;
    std::cin >> x;
    for (size_t i = 0; i < length1; i++)
    {
        double pow_base = length1 - 1 - i;
    }



    int result[3]{};
    // std::cout << "output1:  " << o1 <<  '\n';
    return 0;
}
