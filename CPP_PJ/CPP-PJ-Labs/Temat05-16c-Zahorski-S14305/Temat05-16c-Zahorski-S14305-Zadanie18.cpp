#include <iostream>
using namespace std;
void insert(int* a, int max, int index, int key);
void show_iArray(int arr[], int length);
void insert(int* a, int max, int index, int key)
{
  if (index <= max && index >= 0)
  {
    for (size_t i = index; i < max; i++)
    {
         *(a + i) = *(a + i + 1);
    }
     *(a + index) = key;
  }
  else
  {
      std::cout << "inex is out of array boundary" << '\n';
  }
}

void show_iArray(int arr[], int length)
{
    for (int i = 0; i < length; ++i)
    {
        cout << arr[i];
        cout << endl;
    }
}

int main(int argc, char const *argv[])
{
    int arr_1[] = {1,2,3,0,0,0,0,0,1};
    insert(arr_1,8,3,111);
    show_iArray(arr_1,9);

    return 0;
}
