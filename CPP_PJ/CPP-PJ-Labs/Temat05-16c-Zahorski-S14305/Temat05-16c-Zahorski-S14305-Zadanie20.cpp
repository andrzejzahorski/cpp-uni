#include <iostream>
using namespace std;
void mix(int* a, int m, int* b, int n);
void showSquareArray(int* arr, int size1, int size2);
void show_i_Array(int* myArray, int size);

void show_i_Array(int* arr, int length)
{
    for (int i = 0; i < length; ++i)
    {
        cout << arr[i];
        cout << endl;
    }
}
void showSquareArray(int* a[], int size1, int size2)
{
  for (int i = 0; i < size1; i++)
  {
    for (int j = 0; j < size2; j++)
    {
      std::cout << *(*(a + i) + j) << '\n';
    }
  }
}

void mix(int* a, int m, int* b, int n)
{
    for(int i = 0; i < m; i++)
    {
                if (*(a + i) > *(b))
                {
                    int tmp = *(a + i);
                    *(a + i) = *(b);
                    *(b) = tmp;

                    int k = 0;
                    while (k < n && *(b + k) > *(b + k + 1))
                    {
                        tmp = *(b + k);
                        *(b + k) = *(b + k + 1);
                        *(b + k + 1) = tmp;
                        k++;
                    }
                }
    }
}

int main(int argc, char const *argv[])
{
    int arr_1[3][3] =
    {
            {1,2,3},
            {2,1,3},
            {0,2,1}
    };

    int arr_2[3][3] =
    {
            {1,2,3},
            {2,1,3},
            {3,2,1}
    };
    int arr_3[3][3] = {};
    int* tt1[3];
    int* tt2[3];
    int* tt3[3];

    for (size_t i = 0; i < 3; i++)
    {
        tt1[i] = arr_1[i];
        tt2[i] = arr_2[i];
        tt3[i] = arr_3[i];
    }



    int a[] = {1, 2, 2, 3, 5, 6};
    int b[] = {2, 4, 5, 8};
    mix(a, 6, b, 4);
    std::cout << "arr a" << '\n';
    show_i_Array(a,6);
    std::cout << "arr b" << '\n';
    show_i_Array(b,4);
}
