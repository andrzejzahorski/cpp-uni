#include <iostream>
using namespace std;

void  trans(int* a[], int dim);
void multiply(int* a[], int* b[], int* c[], int dim1, int dim2, int dim3);
void show_i_Arr(int* myArray, int size);
void insert(int* a, int max, int index, int key);
void showSquareArray(int* arr, int size1, int size2);
void showSquareArray(int* a[], int size1, int size2)
{
  for (int i = 0; i < size1; i++)
  {
    for (int j = 0; j < size2; j++)
    {
      std::cout << *(*(a + i) + j) << '\n';
    }
  }
}


void  trans(int* a[], int dim)
{
    int i = 0;
    int j = dim -1;
    while ( i != j  )
    {
        if (*(*(a + i) + j) == *(*(a + j) + i))
        {
            i++;
            j--;
            *(*(a + i) + j) = *(*(a + j) + i);
        }
        else
        {
            std::cout << "NOT SYMETRIC " << '\n';
            break;
        }
        std::cout << "SYMETRIC & TRANSPOSED" << '\n';


    }
}
void multiply(int* a[], int* b[], int* c[], int dim1, int dim2, int dim3)
{
/* zapisującą wynik operacji mnożenia macierzy a  (o wymiarch dim1 x dim2) i b
(o wymiarach dim2 x dim3) do wynikowej macierzy c (o wymiarach dim1 x dim3)*/
    int i, j, k;
    if (dim1 == dim3)
    {
        for(i = 0; i < dim1; ++i)
            for(j = 0; j < dim2; ++j)
                for(k = 0; k < dim1; ++k)
                {
               //mult[i][j] += a[i][k] * b[k][j];
                    *(*(c + i) + j) += ( *(*(a + i) + k) ) * (*(*(b + k) + j)) ;
                }
    }
}
void show_i_Arr(int* myArray, int size)
{
    for (int i = 0; i < size; i++) //go through all elements
        cout<< myArray[i] << " ";
    std::cout << '\n';
}







int main(int argc, char const *argv[])
{
    int arr_1[3][3] =
    {
            {1,2,3},
            {2,1,3},
            {0,2,1}
    };

    int arr_2[3][3] =
    {
            {1,2,3},
            {2,1,3},
            {3,2,1}
    };
    int arr_3[3][3] = {};
    int* tt1[3];
    int* tt2[3];
    int* tt3[3];

    for (size_t i = 0; i < 3; i++)
    {
        tt1[i] = arr_1[i];
        tt2[i] = arr_2[i];
        tt3[i] = arr_3[i];
    }
    trans(tt1, 3);

    multiply(tt1,tt2,tt3,3,3,3);
    showSquareArray(tt3, 3, 3);




    return 0;
}
