//
// Created by Andy on 6/2/17.
//

#include "Ulamek.h"

Ulamek::Ulamek(int licz, int  mian) : licznik(licz), mianownik(mian)
{

}

Ulamek::Ulamek(const Ulamek & ulamek) : licznik(ulamek.licznik), mianownik(ulamek.mianownik)
{

}


Ulamek Ulamek::dodaj(const Ulamek& ulamekDoDodania) const
{
    int b = mianownik * ulamekDoDodania.mianownik;
    int a = licznik * ulamekDoDodania.mianownik + ulamekDoDodania.licznik * mianownik;
    int dzielnik = NWD(a,b);

    return Ulamek(a/dzielnik, b/dzielnik);

}
Ulamek Ulamek::mnoz(const Ulamek& ulamek) const
{
    int a = licznik * ulamek.licznik;
    int b = mianownik * ulamek.mianownik;
    int dzielnik = NWD(a,b);

    return Ulamek(a/dzielnik, b/dzielnik);
}

int Ulamek::NWD(int a, int b) const
{
    while(a!=b)
        if(a>b)
            a-=b; //lub a = a - b;
        else
            b-=a; //lub b = b-a
    return a; // lub b - obie zmienne przechowują wynik NWD(a,b)
}

Ulamek& Ulamek::operator=(const Ulamek& ulamek)
{
    if (this == &ulamek)
        return *this;

    this->licznik = ulamek.licznik;
    this->mianownik = ulamek.mianownik;

    return *this;
}

Ulamek& Ulamek::operator++()
{
    this->licznik += mianownik;

    int a = licznik;
    int  b = mianownik;
    int dzielnik = NWD(a,b);

    licznik /= dzielnik;
    mianownik /= dzielnik;

    return *this;
}

const Ulamek Ulamek::operator++(int)
{
    Ulamek tmp(*this);
    //this->operator++();
    ++(*this);

    return tmp;
}

Ulamek Ulamek::operator-()
{
    return Ulamek(-(licznik), mianownik);
}

std::ostream& operator<<(std::ostream& str, Ulamek ulamek)
{
    str << ulamek.licznik << " / " << ulamek.mianownik;
    return  str;
}

Ulamek operator+(const Ulamek& u, const Ulamek& v)
{
    return u.dodaj(v);
}
