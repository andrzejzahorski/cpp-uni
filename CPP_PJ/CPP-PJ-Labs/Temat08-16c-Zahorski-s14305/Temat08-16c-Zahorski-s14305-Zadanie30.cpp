#include <iostream>
#include <iomanip>

using namespace std;
void usun(int n, int** pas);

void usun(int n, int** pas)
{
	for(int i = 0; i < n; ++i)
		delete[] pas[i];
	delete[] pas;
}

int** pascal(int n)
{
	int** tab = new int*[n+1];
	for(int i = 0; i < n+1; ++i)
	{
		tab[i] = new int[i+1];
		for(int j = 0; j < i+1; ++j)
		{
			if(j == 0 || j == i)
			{
				tab[i][j] = 1;
			}
			else
			{
				tab[i][j] = tab[i-1][j-1] + tab[i-1][j];
			}
		}
	}

	return tab;
}

int main(int argc, char *argv[])
{
	int n = 6;
	int** tab = pascal(n);

	for(int i = 0; i < n+1; ++i)
	{
		for(int s = 0; s < n - i ; ++s)
			cout << setw(2) << " ";
		for(int j = 0; j < i+1; ++j)
		{
			cout << setw(2) << tab[i][j] << setw(2) << " ";
		}
		cout << endl;
	}

	usun(n,tab);


	return 0;
}
