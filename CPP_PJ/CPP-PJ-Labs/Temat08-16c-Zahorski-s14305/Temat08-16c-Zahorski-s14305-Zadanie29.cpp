#include <iostream>
using namespace std;

char** read(int n);

char** read(int n)
{
    char** inputRead = new char*[n];
    for (int i = 0; i < n; ++i)
    {
        inputRead[i] = new char[1];
        inputRead[i][0] = '\0';

        char c;
        while ( (c = cin.get()) != '\n' )
        {
            int len = 1 + strlen(inputRead[i]);
            char* tmp = new char[len + 1];
            strncpy(tmp,inputRead[i],len);
            tmp[len - 1] = c;
            tmp[len] = '\0';
            delete[] inputRead[i];
            inputRead[i] = tmp;
        }
    }
    return inputRead;
}

int main()
{
    char** napisy = read(3);
    for (int i = 0; i < 3; ++i)
    {
        cout << napisy[i];
        cout << endl;
        delete[] napisy[i];
    }
    delete[] napisy;
    return 0;
}