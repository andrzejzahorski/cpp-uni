#include <iostream>
using namespace std;
 int** test(int* tab, int n);

  int** test(int* tab, int n)
  {
      int *a = new int [3];
      a[0] = 0;
      int last_a_size = 3;

      for (size_t i = 1; i <= n ; i++)
      {
          if ((n % i) == 0)
          {
              if ((last_a_size - 1) == a[0])
              {
                  int* tmp = new int[2*last_a_size];

                  for (size_t j = 0; j < last_a_size; j++)
                  {
                      tmp[j] = a[j];
                  }
                  delete[] a;
                  a = tmp;
                  last_a_size *= 2;
              }
              a[ ++a[0] ] = i;

          }
      }

      int* b = new int[1];
      b[0] = 0;
      int last_b_size = 1;

      for (int k = 0; k < n ; ++k)
      {
          if (tab[k] % 2 == 0 )
          {
              if ((last_b_size - 1) == b[0])
              {
                  int* tmp2 = new int[2 * last_b_size];

                  for (int i = 0; i < last_b_size; ++i)
                  {
                      tmp2[i] = b[i];
                  }
                  delete[] b;
                  b = tmp2;
                  last_b_size *= 2;
              }

              b[ ++b[0] ] = tab[k];
          }
      }

      int** out = new int* [2];
      out[0] = a;
      out[1] = b;

      return out;

  }

int main(int argc, char const *argv[])
{
    const int n = 10;
    int* tab = new int[n];

    for (size_t i = 0; i < n; i++)
    {
        *(tab + i) = rand()%10;
        std::cout << tab[i] << " ";

    }
    std::cout  << '\n';

     int** pTab = test(tab,n);

    for (int j = 0; j < 2 ; ++j)
    {
        for (int i = 0; i <= pTab[j][0] ; ++i)
        {
            cout << pTab[j][i] << " ";
        }
        cout << endl;
    }
    for (int k = 0; k < 2 ; ++k)
    {
        delete[] pTab[k];
    }
    delete[] pTab;
    delete[] tab;
    return 0;
}
