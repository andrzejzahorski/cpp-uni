

#include <iostream>
#include <cstdarg>
#include <math.h>


double povalue(double x, int n, ...);
void printWiel(int wiel[], int n);
void multiply(int* coeff1, int m, int* coeff2, int n, int* result);
void add(int* coeff1, int m, int* coeff2, int n, int* result);

void add(int* coeff1, int m, int* coeff2, int n, int* result)
{
    int size = (m > n) ? m : n;

    for (int i = 0; i<m; i++)
        result[i] = coeff1[i];

    for (int i=0; i<n; i++)
        result[i] += coeff2[i];
    printWiel(result,size);
}


void printWiel(int wiel[], int n)
{
    for (int i=0; i<n; ++i)
    {
        std::cout << wiel[i];
        if (i != 0)
            std::cout << "x^" << i ;
        if (i != n-1)
            std::cout << " + ";
    }


    std::cout << "\n" << "Współczynniki:\n";

    for (int j = 0; j < n ; ++j)
    {
        if (j != 0)
        {
            std::cout << "Współczynnik: "  << wiel[j] << std::endl;
        }
    }

}


double povalue(double x, int n, ...)
{
    double output = 0;
    va_list ap;
    va_start(ap, n);
    for ( int i = 0; i < n; i++ )
    {
            double pow_base = n - 1 - i;
            // std::cout << "pow_base  " << pow_base << '\n';
            double now = va_arg(ap,double);
            if (n - 1 - i >= 1)
            {
                double pow_arg = pow(x, pow_base);
                // std::cout << "pow_arg" << pow_arg << '\n';
                output += now * pow_arg;
            }
            else
            {
                output += now;
            }
    }
  va_end ( ap );

  return output;
}

 void multiply(int* coeff1, int m, int* coeff2, int n, int* result)
 {
     for (int i=0; i<m; i++)
     {
         for (int j = 0; j < n; j++)
             result[i + j] += coeff1[i] * coeff2[j];
     }

     printWiel(result,m+n-1);


 }

int main(int argc, char const *argv[])
{
    double o1 = povalue(-1.0, 3, 4.0, -2.0, 1.0);

    std::cout << "povalue: " << o1 << std::endl;
    int w1[] = {1 , 2, 3};
    int w2[] = {4, 5, 6};
    const int length1 = sizeof(w1)/sizeof(w1[0]);
    const int length2 = sizeof(w2)/sizeof(w2[0]);

    int arr_result[length1 + length2 - 1] = {};

    int w3[] = {3 , 3, 3};
    int w4[] = {4, 5, 6};
    const int length3 = sizeof(w1)/sizeof(w1[0]);
    const int length4 = sizeof(w2)/sizeof(w2[0]);

    multiply(w3, length1, w2, length2, arr_result);
    add(w1,length1,w2,length2,arr_result);



    return 0;
}
