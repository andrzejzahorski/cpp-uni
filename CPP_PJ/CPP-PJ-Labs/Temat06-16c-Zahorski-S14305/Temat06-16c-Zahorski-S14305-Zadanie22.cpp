#include <iostream>
#include <map>
using namespace std;


int main(int argc, char const *argv[])
{
    std::string napis = "";
    std::cout << "Insert letters adding DOT at the end," 
    "and press ENTER" << '\n';
    getline(std::cin, napis, '.');
    int length = napis.length();
    std::string alphaOnly = "";

    for (size_t i = 0; i < length; i++)
    {
        if(isalpha(napis.at(i)))
        {
            alphaOnly += napis.at(i);
        }
    }

    std::map<char,int> mapa;
    std::map<char,int>::iterator it;
    for (size_t j = 0; j < alphaOnly.length(); j++)
    {
        char alphaSign = alphaOnly.at(j);
        it = mapa.find(alphaSign);
        if (it == mapa.end())
        {
            mapa[alphaSign] = 1;
        }
        else
        {
            mapa[alphaSign] += 1;
        }
    }

    for (it = mapa.begin(); it != mapa.end(); it++)
    {
    //cout <<  << it->first << endl << "Value: " << it-> second << endl;
        std::cout <<  it->first << " ";
        for (size_t i = 0; i < it->second; i++)
        {
            std::cout << "#";
        }
        std::cout << " " << it->second << '\n';
    }

    return 0;
}
