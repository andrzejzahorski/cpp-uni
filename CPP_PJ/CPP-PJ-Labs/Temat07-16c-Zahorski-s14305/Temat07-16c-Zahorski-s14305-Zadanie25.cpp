
template<typename T>
T min_maks(T* arr, int size, bool(*F)(T,T))
{
    T out = *arr;
    for (size_t i = 1; i < size; i++)
    {
        out  = F(out, arr[i]) ? out : arr[i];
    }
    return out;
}

template<typename T>
 bool mniejsze(T pierwszy, T drugi)
 {
    return pierwszy < drugi;
 }

 template<typename T>
 bool wieksze(T pierwszy, T drugi)
 {
    return pierwszy > drugi;
 }

 int main(int argc, char const *argv[])
 {
    bool (*f)(int,int) = &mniejsze;
    int arr_1[] = {1,2,3};
    int min = min_maks(arr_1,3 , f);
    std::cout << "min" << min << '\n';
     return 0;
 }
