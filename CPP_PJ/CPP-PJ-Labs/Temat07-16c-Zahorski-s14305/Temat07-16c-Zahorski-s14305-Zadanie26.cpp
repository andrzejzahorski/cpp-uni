#include <iostream>

template<typename T>
int binSearch(T arr[], T value, int begin, int end)
{
    while (begin <= end)
    {
        int mid = begin + (end - begin) / 2;
        std::cout << mid << "\n";
        if (arr[mid] == value)
            return mid;
        else if (arr[mid] < value)
            begin = mid + 1;
        else
            end  = mid - 1;
    }
    return -1;
}

int main(int argc, char const *argv[]) {
    int arr_2[] = {2,3,4,5};
    int address = binSearch(arr_2, 4, 0, 3);
    std::cout << "address " << address << '\n';
    return 0;
}
