#include <iostream>
#include <stdio.h>
#include <ctype.h>


char* odwroc(char* nap, int n)
{
    int size = 0;
    char* letter = nap;
    while (*letter != '\0') {
        size++;
        letter++;
    }
    int med = size / 2;
    for (size_t i = 0; i <= med; i++)
    {
        if (i != n && size - i - 1 !=  n)
        {
            char tmp = *(nap + i);
            nap[i] = nap[size - i - 1];
            nap[size - i - 1] = tmp;
        }
    }
    return nap;
}

char* duze(char* nap, int n)
{
    int letter = 0;

    while (*(nap + letter) != '\0')
    {
        if (letter != n && isalpha( *(nap + letter)) )
            toupper(*(nap + letter));
        letter++;
    }
    return nap;
}

typedef char* (*tabFun[])(char*, int);



void modyfikuj(tabFun tab, int size, char* napis, int number)
{
    for (size_t i = 0; i < size; i++)
    {
        tab[i](napis, number);
    }
}

int main(int argc, char const *argv[])
{
    tabFun arr =
            {
                    odwroc,
                    duze,
                    [](char* nap, int num ) -> char*
                    {
                        int letter = 0;

                        while (*(nap + letter) != '\0')
                        {
                            if (letter != num && isalpha( *(nap + letter)) )
                                tolower(*(nap + letter));
                            letter++;
                        }
                        return nap;
                    }
            };
    char word[] = "kotek";
    int i = 1;
    std::cout << word << '\n';
    modyfikuj(arr, 3, word, 2);
    std::cout << "zmienione kocisko " << word << '\n';
    return 0;
}
